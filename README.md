# QuickBase interview task instructions

## Steps
Install project dependencies. I used virtualenvironment wrapper for this.Create virtual environment and then install requirements in it

* mkvirtualenv task -p /usr/bin/python2.7 
* pip install -r requirements.txt

## Run tests
* python -m unittest app.test.tests

## Run create contact script with -h for details and it's usage
* python createcontact.py  -h


