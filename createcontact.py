import os
import logging
import argparse
from app.utils.clients import GithubClient, FreshDeskClient
from app.utils.errors import (GitHubBadCredentialsException,
                              FreshDeskValidationException,
                              FreshDeskConflictException,
                              GeneralClientException,
                              FreshDeskBadCredentialsException,
                              GitHubNotFoundException)
from app.models.github_user import GithubUser

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)


def main(options):
    freshdesk_domain = options.freshdesk_subdomain
    github_user = options.github_user

    github_token = os.environ.get("GITHUB_TOKEN", "putyourtokenhere")
    freshdesk_token = os.environ.get("FRESHDESK_TOKEN", "putyourtokenhere")
    github_client = GithubClient(github_token, github_user)

    try:
        user_data = github_client.get_user_info()
    except GitHubBadCredentialsException as e:
        logging.warning(e.message)
        return
    except GitHubNotFoundException as e:
        logging.warning(e.message)
        return
    except GeneralClientException as e:
        logging.warning(e.message)
        return

    user = GithubUser(**user_data)
    freshdesk_client = FreshDeskClient(freshdesk_token, freshdesk_domain)
    payload = user.to_freshdesk_contact()

    try:
        freshdesk_client.create_contact(payload)
    except FreshDeskBadCredentialsException as e:
        logging.warning(e.message)
        return
    except FreshDeskValidationException as e:
        logging.warning(e.message)
        return
    except FreshDeskConflictException:
        logging.info("Contact already exists! Perform update...")
        contact_data = freshdesk_client.get_contact(user.id)
        contact_id = contact_data[0].get("id")
        data = freshdesk_client.update_contact(contact_id, payload)
        if data:
            logging.info("Contact udpated...")
    except GeneralClientException as e:
        logging.warning(e.message)
    else:
        logging.info("Contact created...")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Creates a freshdesk contact. If contact exists performs update",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument("-u", "--user", type=str, dest="github_user",
                        help="Github user name", required=True)

    parser.add_argument("-sd", "--sub_domain", type=str, dest="freshdesk_subdomain",
                        help="Freshdesk subdomain used to call api for your freshdesk account data", required=True)

    options = parser.parse_args()
    main(options)