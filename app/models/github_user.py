class GithubUser(object):
    def __init__(self, **kwargs):
        self.id = kwargs.get("id")
        self.email = kwargs.get("email")
        self.name = kwargs.get("name")

    def _clean_empty_values(self, data):
        return dict((k, v) for k, v in data.iteritems() if v)

    def to_freshdesk_contact(self):
        payload = {
            "name": self.name,
            "email": self.email,
            "unique_external_id": str(self.id)

        }

        payload = self._clean_empty_values(payload)

        return payload
