import unittest
from mock import patch
from ..utils.clients import GithubClient, FreshDeskClient
from ..utils.errors import (GitHubBadCredentialsException,
                            FreshDeskValidationException,
                            FreshDeskConflictException,
                            FreshDeskBadCredentialsException,
                            GitHubNotFoundException)
from ..models.github_user import GithubUser


class MockResponse:
    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data

    def raise_for_status(self):
        pass


class GithubClientTests(unittest.TestCase):
    def setUp(self):
        self.client = GithubClient(token="some-token", user_name="batvas")

    def test_get_user_info_bad_credentials(self):
        mock_response = MockResponse({}, 401)
        patch("requests.Session.get", return_value=mock_response).start()
        self.assertRaises(GitHubBadCredentialsException, self.client.get_user_info)

    def test_get_user_info_not_found(self):
        mock_response = MockResponse({}, 404)
        patch("requests.Session.get", return_value=mock_response).start()
        self.assertRaises(GitHubNotFoundException, self.client.get_user_info)

    def test_get_user_info_success(self):
        mocked_data = {"name": "test", "id": 1, "email": "someemail@abv.bg"}
        mock_response = MockResponse(mocked_data, 201)
        patch("requests.Session.get", return_value=mock_response).start()
        self.assertEqual(mocked_data, self.client.get_user_info())

    def tearDown(self):
        patch.stopall()


class FreshDeskClientTests(unittest.TestCase):
    def setUp(self):
        self.client = FreshDeskClient(token="some-token", domain="some-domain")

    def test_check_for_expected_errors_invalid_payload(self):
        mock_response = MockResponse({}, 400)
        self.assertRaises(FreshDeskValidationException, self.client._check_for_expected_errors, mock_response, {})

    def test_check_for_expected_errors_conflict_error(self):
        mock_response = MockResponse({}, 409)
        self.assertRaises(FreshDeskConflictException, self.client._check_for_expected_errors, mock_response, {})

    def test_check_for_expected_errors_bad_credentials(self):
        mock_response = MockResponse({}, 401)
        self.assertRaises(FreshDeskBadCredentialsException, self.client._check_for_expected_errors, mock_response, {})

    def test_create_contact_success(self):
        mocked_data = {"name": "test", "id": 1, "email": "someemail@abv.bg"}
        mock_response = MockResponse(mocked_data, 201)
        patch("requests.Session.post", return_value=mock_response).start()
        self.assertEqual(mocked_data, self.client.create_contact({}))

    def test_create_contact_invalid_payload(self):
        json_data = {"errors": []}
        mock_response = MockResponse(json_data, 400)
        patched = patch.object(FreshDeskClient, '_check_for_expected_errors').start()
        patch("requests.Session.post", return_value=mock_response).start()
        self.client.create_contact({})
        patched.assert_called_with(mock_response, json_data)

    def test_update_contact_invalid_payload(self):
        json_data = {"errors": []}
        mock_response = MockResponse(json_data, 400)
        patched = patch.object(FreshDeskClient, '_check_for_expected_errors').start()
        patch("requests.Session.put", return_value=mock_response).start()
        self.client.update_contact(1, {})
        patched.assert_called_with(mock_response, json_data)

    def test_get_contact_invalid_payload(self):
        json_data = {"errors": []}
        mock_response = MockResponse(json_data, 400)
        patched = patch.object(FreshDeskClient, '_check_for_expected_errors').start()
        patch("requests.Session.get", return_value=mock_response).start()
        self.client.get_contact(1)
        patched.assert_called_with(mock_response, json_data)

    def tearDown(self):
        patch.stopall()


class TestUser(unittest.TestCase):
    def setUp(self):
        self.test_data = {"id": 1, "email": "someemail@abv.bg", "name": "vasil"}

    def test_clean_empty_values(self):
        user = GithubUser()
        self.assertEqual({"id": 1}, user._clean_empty_values({"id": 1, "email": "", "name": None}))
        self.assertEqual({}, user._clean_empty_values({}))
        self.assertEqual(self.test_data, user._clean_empty_values(self.test_data))

    def test_to_freshdesk_contact(self):
        user = GithubUser(**self.test_data)
        expected = {"unique_external_id": "1", "email": "someemail@abv.bg", "name": "vasil"}
        self.assertEqual(expected, user.to_freshdesk_contact())


def suite():
    test_suite = unittest.TestSuite()
    test_suite.addTests(unittest.makeSuite(GithubClientTests, "test"))
    test_suite.addTests(unittest.makeSuite(FreshDeskClientTests, "test"))
    test_suite.addTests(unittest.makeSuite(TestUser, "test"))

    return test_suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())