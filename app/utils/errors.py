from requests import HTTPError


class GeneralClientException(HTTPError):
    pass


class GitHubBadCredentialsException(Exception):
    pass


class GitHubNotFoundException(Exception):
    pass


class FreshDeskValidationException(Exception):
    pass


class FreshDeskConflictException(Exception):
    pass


class FreshDeskBadCredentialsException(Exception):
    pass
