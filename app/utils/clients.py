import json
from requests import Session, HTTPError
from requests.auth import HTTPBasicAuth
from app.utils.errors import (GitHubBadCredentialsException,
                              GeneralClientException,
                              FreshDeskValidationException,
                              FreshDeskConflictException,
                              FreshDeskBadCredentialsException,
                              GitHubNotFoundException)


class BaseClient(object):
    def __init__(self, token, verify):
        self._session = Session()
        self._session.verify = verify
        self._session.headers.update({"Content-Type": "application/json"})

    def _check_for_other_http_errors(self, response):
        try:
            response.raise_for_status()
        except HTTPError as e:
            raise GeneralClientException("{}".format(e))


class GithubClient(BaseClient):
    def __init__(self, token, user_name, verify=True):
        super(GithubClient, self).__init__(token, verify)
        self._session.headers.update({"Authorization": "token %s" % token})
        self.domain = "https://api.github.com"
        self.user_name = user_name

    def get_user_info(self):
        url = self.domain + "/users/" + self.user_name
        response = self._session.get(url)

        try:
            data = response.json()
        except ValueError:
            data = {}

        if response.status_code == 401:
            raise GitHubBadCredentialsException("Fail to connect to Github, bad credentials!")
        elif response.status_code == 404:
            raise GitHubNotFoundException("User:{} not found!".format(self.user_name))

        """
            TODO handle properly other errors as well,
            https://developer.github.com/v3/
        """
        self._check_for_other_http_errors(response)
        return data


class FreshDeskClient(BaseClient):
    def __init__(self, token, domain, verify=True):
        super(FreshDeskClient, self).__init__(token, verify)
        self.base = domain.rstrip("/") + "/api/v2"
        self._session.auth = HTTPBasicAuth(token, 'unused')

    def _check_for_expected_errors(self, response, data):
        errors = data.get("errors") if "errors" in data else {}
        error_msg = ""
        if errors:
            error_msg = "{}".format(errors)

        if response.status_code == 401:
            raise FreshDeskBadCredentialsException(error_msg or "Fail to connect to FreskDesk, bad credentials!")
        elif response.status_code == 400:
            raise FreshDeskValidationException(error_msg or "Invalid payload")
        elif response.status_code == 409:
            raise FreshDeskConflictException(error_msg or "Resource already exists")

        """
            TODO handle properly other errors as well,
            https://developers.freshdesk.com/api/#error
        """

    def create_contact(self, payload):
        url = self.base + "/contacts"
        response = self._session.post(url, data=json.dumps(payload))
        try:
            data = response.json()
        except ValueError:
            data = {}

        self._check_for_expected_errors(response, data)
        self._check_for_other_http_errors(response)
        return data

    def update_contact(self, contact_id, payload):
        url = self.base + "/contacts/" + str(contact_id)
        response = self._session.put(url, data=json.dumps(payload))

        try:
            data = response.json()
        except ValueError:
            data = {}

        self._check_for_expected_errors(response, data)
        self._check_for_other_http_errors(response)
        return data

    def get_contact(self, github_id):
        url = self.base + "/contacts?unique_external_id=" + str(github_id)
        response = self._session.get(url)

        try:
            data = response.json()
        except ValueError:
            data = {}

        self._check_for_expected_errors(response, data)
        self._check_for_other_http_errors(response)

        return data
